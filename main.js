
function endpointDatosPorId() {
  const xhttp = new XMLHttpRequest();
  xhttp.onload = function() {
    document.getElementById("demo").innerHTML =
    this.responseText;
  }
  xhttp.open("GET", "http://localhost:5294/cliente/listarPorId?id=1");
  xhttp.send();
}


function saveClient() {
  var frm=$( "#frm" ); 
  var datos = frm.serialize();  
  var request = $.ajax({
      type:"POST",
      beforeSend: function(request) {
        request.setRequestHeader("Content-Type", "application/json; charset=utf8");
      },
      url: "http://localhost:5294/cliente/guardar",    
      data: JSON.stringify({
        "idCliente": 0,
        "nombre": document.getElementById('name').value,
        "correo": document.getElementById('email').value,
        "telefono": document.getElementById('phone').value,
        "descProblema": document.getElementById('textarea1').value
      }),  
      success:function(datos){ 
        alert(request.responseJSON.message);
        document.getElementById("frm").reset();
        response = datos;
    },              
      dataType: "json"           
  });
}

function updateClient() {
  var frm=$( "#frm" ); 
  var datos = frm.serialize();  
  var request = $.ajax({
      type:"PUT",
      beforeSend: function(request) {
        request.setRequestHeader("Content-Type", "application/json; charset=utf8");
      },
      url: "http://localhost:5294/cliente/editar",    
      data: JSON.stringify({
        "idCliente": document.getElementById('idCliente').value,
        "nombre": document.getElementById('name').value,
        "correo": document.getElementById('email').value,
        "telefono": document.getElementById('phone').value,
        "descProblema": document.getElementById('textarea1').value
      }),  
      success:function(datos){ 
        alert(request.responseJSON.message);
        $("#tbodyid").empty();
        getList();
    },              
      dataType: "json"           
  });

  
}

function getList() {

  var request = $.ajax({
      type:"GET",
      beforeSend: function(request) {
        request.setRequestHeader("Content-Type", "application/json; charset=utf8");
      },
      url: "http://localhost:5294/cliente/listar",     
      success:function(datos){ 
        var objItems = datos.result.cliente;
        var tableContent = '';
        
        for (var i = 0; i < objItems.length; i++) {
          tableContent += '<tr>';
          tableContent += '<td>' + objItems[i].nombre + '</td>';
          tableContent += '<td>' + objItems[i].correo + '</td>';
          tableContent += '<td>' + objItems[i].telefono + '</td>';
          tableContent += '<td>' + objItems[i].descProblema + '</td>';
          tableContent += '<td>' + "<div><a class='waves-effect waves-light modal-trigger' onclick='editClient("+JSON.stringify(objItems[i])+");'><i class='large material-icons' style='font-size:35px; cursor: pointer;'>mode_edit</i></a></div>" + '</td>';
          tableContent += '</tr>';
        }
        $('#clientTab tbody').append(tableContent);

        console.log(request.responseJSON);
    } 
  });
}

function validateEmail(email) { //Validates the email address
  var emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return emailRegex.test(email);
}

function checkPhone() {
  var phone = document.getElementById('phone');
  var phoneNum = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/; 

      if (phone.value == "") {
        alert( "Teléfono es un campo requerido");
        return false;
    }
    else if (!phone.value.match(phoneNum)) {
        alert( "Número de telefono no válido");
        return false;
    }else{
      return true;
    }
  }

function doValidate() {
  var name = document.getElementById('name');
  var textarea = document.getElementById('textarea1');

  let nameValid;
  let emailValid;
  let phoneValid;
   let textareaValid;

  if(!(name.value != null && (name.value).trim() != "")){
    alert( "Nombre es un campo requerido");
        return false;
  }else{
    nameValid = true;
  }

  emailValid = validateEmail();
  phoneValid = checkPhone();

  if(!(textarea.value != null && (textarea.value).trim() != "")){
    alert( "Descripción del problema es un campo requerido");
        return false;
  }else{
    textareaValid = true;
  }

  if(nameValid && emailValid && phoneValid && textareaValid){
    saveClient();
  }

}

function validateEmail() {

  var email = document.getElementById('email');
  var mailFormat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

  if (email.value == "") {
      alert( "Correo es un campo requerido");
      return false;
  }
  else if (!mailFormat.test(email.value)) {
      alert( "Direccion de correo no válida");
      return false;
  }else{
    return true;
  }


}

function redirectUrl() {
  window.open("editRows.html");
}

function editClient(obj){
  $('#modal1').modal('open');
  $('#idCliente').val(obj.idCliente);
  $('#name').val(obj.nombre);
  $('#email').val(obj.correo);
  $('#phone').val(obj.telefono);
  $('#textarea1').val(obj.descProblema);


}

$(document).ready(function(){
  $('.modal').modal();
});
     